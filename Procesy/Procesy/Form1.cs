﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Procesy
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Lista obiektów typu proces
        /// </summary>
        List<Processs> lproces = new List<Processs>();

        //https://www.experts-exchange.com/questions/26585766/Determine-the-active-Window-of-a-process.html
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        /// <summary>
        /// Funkcja sprawdzająca czy proces jest na pierwszym planie
        /// </summary>
        /// <param name="processname"></param>
        /// <returns></returns>
        public bool ProcessIsFocused(string processname)
        {
            if (processname == null || processname.Length == 0)
            {
                throw new ArgumentNullException("processname");
            }

            var runninProcesses = Process.GetProcessesByName(processname);
            var activeWindowHandle = GetForegroundWindow();

            foreach (Process process in runninProcesses)
            {
                if (process.MainWindowHandle.Equals(activeWindowHandle))
                {
                    return true;
                }
            }

            // Process was not found or didn't had the focus.
            return false;
        }

        public Form1()
        {
            InitializeComponent();
            timer2.Start();
        }  
        /// <summary>
        /// Zdarzenie wywoływane co sekunde dodające do listy aktualny proces
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick_1(object sender, EventArgs e)
        {
            var lista_procesow = Process.GetProcesses().ToList();

            lista_procesow = lista_procesow.OrderBy(p => p.ProcessName).ToList();

            var ostatni = lista_procesow[1];
            foreach (var s in lista_procesow)
            {
                if (ostatni.ProcessName == s.ProcessName) continue;
                ostatni = s;


                if (!ProcessIsFocused(s.ProcessName)) continue;
                var proces = lproces.FirstOrDefault(p => p.nazwa == s.ProcessName && p.ID == s.Id);
                if (proces != null)
                {
                    var indeks = lproces.FindIndex(p => p.nazwa == s.ProcessName && p.ID == s.Id);

                    var czas = lproces[indeks].czas.Add(new TimeSpan(0, 0, 0, 1));

                    lproces[indeks].czas = czas;

                }
                else lproces.Add(new Processs(new TimeSpan(0, 0, 0, 1), s.ProcessName, s.Id));
                break;
            }



        }
        /// <summary>
        /// Zdarzenie na wciśniecie przycisku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            var sw = new StreamWriter("Dane.bin");
            dataGridView2.Rows.Clear();

            chart1.Series.Clear();

            foreach (var s in lproces)
            {
                sw.WriteLine(s.nazwa + ";" + s.czas.ToString("g"));

                Series ser = chart1.Series.Add(s.nazwa);
                ser.Points.Add(s.czas.Seconds);
                dataGridView2.Rows.Add(s.nazwa, s.czas.ToString("g"));
            }
            if (chart1.Series.Count != 0) chart1.ChartAreas[0].AxisY.Maximum = lproces.Max(s => s.czas.Seconds);

            sw.Close();
        }
    }
}