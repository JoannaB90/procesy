﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading.Tasks;

namespace Procesy
{
    class Processs
    {
        /// <summary>
        /// Pole zawerające identyfikator procesu
        /// </summary>
        public int ID;
        /// <summary>
        /// Pole zawierające czas trwania procesu
        /// </summary>
        public TimeSpan czas;
        /// <summary>
        /// Pole zawierające nazwę procesu 
        /// </summary>
        public string nazwa;
        /// <summary>
        /// Konstruktor inicjalizujący
        /// </summary>
        /// <param name="c"></param>
        /// <param name="n"></param>
        /// <param name="i"></param>
        public Processs(TimeSpan c, string n,int i)
        {
            ID = i;
            czas = c;
            nazwa = n;
        }
}
}
